<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>
<?php global $post ?>

	<main class="<?php echo $post->post_type; ?>" id="<?php echo $post->post_name; ?>">

		<?php flexible_layout(); ?>

	</main>

<?php get_footer(); ?>