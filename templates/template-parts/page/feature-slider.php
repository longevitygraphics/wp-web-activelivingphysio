<div class="top-banner <?php echo (is_front_page() ? "home-banner" :  "");  ?>">
	<?php $top_banner = get_field('top_banner', 'option'); ?>

	<?php

	if(is_tax()){
		$term = get_queried_object();
		$images = get_field('top_banner', $term);
		$text_overlay = get_field('text_overlay', $term);
	}else{
		$images = get_field('top_banner');
		$text_overlay = get_field('text_overlay');
	}
	
	$size = 'full'; // (thumbnail, medium, large, full or custom size)

	if( $images ): ?>
	    <div class="gallery">
	        <?php foreach( $images as $image ): ?>
	            <div>
	            	<?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
	            </div>
	        <?php endforeach; ?>
	    </div>
	<?php else: ?>

		<div class="gallery">
			<?php 
				if($post->post_type == 'page'): 
				$page_default_banner = get_field('page_default_banner', 'option');
			?>
				<?php if($page_default_banner): ?>
					<img src="<?php echo $page_default_banner['url']; ?>" alt="<?php echo $page_default_banner['alt']; ?>">
				<?php endif; ?>
					
			<?php elseif($post->post_type == 'post'): 
				$blog_default_banner = get_field('blog_default_banner', 'option');
			?>

				<?php if($blog_default_banner): ?>
					<img src="<?php echo $blog_default_banner['url']; ?>" alt="<?php echo $blog_default_banner['alt']; ?>">
				<?php endif; ?>

			<?php elseif($post->post_type == 'service'): 
				$service_default_banner = get_field('service_default_banner', 'option');
			?>

				<?php if($service_default_banner): ?>
					<img src="<?php echo $service_default_banner['url']; ?>" alt="<?php echo $service_default_banner['alt']; ?>">
				<?php endif; ?>

			<?php elseif($post->post_type == 'project'): 
				$project_default_banner = get_field('project_default_banner', 'option');
			?>

				<?php if($project_default_banner): ?>
					<img src="<?php echo $project_default_banner['url']; ?>" alt="<?php echo $project_default_banner['alt']; ?>">
				<?php endif; ?>
			<?php else: ?>
				<img src="<?php echo get_stylesheet_directory_uri() . '/assets/dist/images/service-banners.jpg'  ?>" alt="Default Banner" style="height: 	800px; object-fit: cover;">
			<?php endif; ?>
		</div>

	<?php endif; ?>


	<?php
		
		$form_active = get_field('form_active');
		$form_overlay = get_field('form_overlay', 'option');
	?>
	<?php 	if($text_overlay): ?>
	<div class="overlay">
		<div class="text-content">
			<div>
				<?php echo $text_overlay; ?>
			</div>
		</div>
		<?php if($form_active == 1): ?>
		<div class="form_content">
			<?php echo do_shortcode($form_overlay); ?>
		</div>
		<?php endif; ?>
	</div>
	<?php 	endif; ?>
</div>