<?php 
/**
 * Text Block Layout
 *
 */

use Longevity\Framework\Helper; 


?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->
	<?php
		$align_items_vertical	= get_sub_field('align_items_vertical'); 
		$align_items_horizontal = get_sub_field('align_items_horizontal'); 

		$svg_image = get_sub_field('svg_image');
		$svg_content = get_sub_field('svg_content');
		$svg_link = get_sub_field('svg_link');


		$svg_url = $svg_image['url'];
		$svg_object = file_get_contents($svg_url );






	?>

	<div class="d-flex flexible_svg <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php echo $align_items_vertical; ?> <?php echo $align_items_horizontal; ?>">
		<div class="col-12 d-flex flex-column <?php echo $align_items_horizontal; ?> <?php  echo $align_items_vertical ?>">
			<a href="<?php echo $svg_link['url']; ?>" target="<?php echo $svg_link['target'] ?>">
			<?php 
			echo $svg_object;
			echo $svg_content;
			
			?>
			</a>

		</div>
	</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>
