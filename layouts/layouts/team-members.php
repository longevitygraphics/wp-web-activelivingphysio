<?php 
/**
 * Team Members Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->
		<?php

		$post_cat = get_sub_field('team_category');
		$grid_width = get_sub_field('grid_responsive');

		$w_lg = (int)$grid_width['item_per_row_large'];
		$w_md = (int)$grid_width['item_per_row_medium'];
		$w_sm = (int)$grid_width['item_per_row_small'];
		$w_xs = (int)$grid_width['item_per_row_extra_small'];

		$show_bio = get_sub_field('show_bio');
		$bio_choice = in_array('yes', $show_bio);


		

		$query = new WP_Query(array ('post_type' => 'team-member', 'nopaging' => true, 'tax_query' => array(array( 'taxonomy' => 'team-category', 'field' => 'slug', 'terms' => array( $post_cat )))));
		if($query->have_posts()) {
			echo '<div class="team-members d-flex justify-content-center align-items-center container flex-wrap">';
			while ($query->have_posts() ):

				$query->the_post();

				$team_image = get_field('team_image');
				$team_name = get_field('team_name');
				$team_title = get_field('team_title');
				$team_excerpt = get_field('team_excerpt');
				$flex_direction = (($bio_choice) ? '' : 'flex-column');
				$justify_content = (($bio_choice) ? 'justify-content-start' : 'justify-content-center');
				$align_content = (($bio_choice) ? 'align-items-start' : 'align-items-center');
				global $post;
				$post_slug = $post->post_name;
				$team_page = get_field('team_page', 'options');
				?>

				<div class="team-member d-flex justify-content-center justify-content-md-start  align-items-center align-items-md-start col-<?php echo $w_xs; ?> col-sm-<?php echo $w_sm; ?> col-md-<?php echo $w_md; ?> col-lg-<?php echo $w_lg; ?> <?php echo $post_cat; ?> <?php echo $flex_direction; ?> show_bio-<?php echo $bio_choice ?> <?php if($bio_choice) echo "py-4"; ?>" id="<?php echo $post_slug  ?>">
				
				<?php if($show_bio): ?>
					<div class="row">
						<div class="col-12 col-md-3">
				<?php endif; ?>

						<div class="team-image"> 
							<a href="<?php echo (is_front_page() ? $team_page."#".$post_slug :  "");  ?>">
								<img src="<?php echo $team_image['url']; ?>" alt="<?php echo $team_image['alt']; ?>">
							</a>
						</div>

				
				<?php if($show_bio): ?>
					</div>
						<div class="col-12 col-md-9">
						<div class="team-content-bio d-flex flex-column justify-content-start align-items-start pt-2">
							<div class="team-name"> <?php echo $team_name ?></div>
							<div class="team-title"><?php echo $team_title ?></div>
							<div class="team-bio"><?php echo $team_excerpt ?></div>
						</div>
						</div>
					</div>
					</div>


				<?php else: ?>
					<div class="team-content d-flex flex-column justify-content-start align-items-start ">
						<div class="team-name"><?php echo $team_name ?></div>
						<div class="team-title"><?php echo $team_title ?></div>
					</div>
				</div>	


				<?php endif; ?>
			<?php endwhile; ?>
		<?php
		wp_reset_postdata(); 
			
			
		}
		else {
			echo "No Posts Found";
		}

		?>
</div>




<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>