<?php 
/**
 * Text Block Layout
 *
 */
?>
<?php

	get_template_part('/layouts/partials/block-settings-start');
	
    $background_image = get_sub_field('overlay_background_image');
    $opacity = get_sub_field('background_image_opacity');
    $main_content = get_sub_field('opacity_text_main_content');
    $min_height = get_sub_field('background_min_height');
?>
<!--------------------------------------------------------------------------------------------------------------------------------->
<div class="d-flex flexible-opacity-text <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
    <div class="top-banner">
        <div class="gallery" style="opacity: <?php echo $opacity ?>">
            <div class="">
                <img src="<?php echo $background_image['url'] ?>" alt="<?php echo $background_image['alt'] ?>">
            </div>
        </div>
        <div class="overlay">
            <div class="" style="min-height: <?php echo $min_height; ?>px">
                <div>
                    <?php echo $main_content ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!--------------------------------------------------------------------------------------------------------------------------------->
<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>
