<?php
//Custom Post Types
function create_post_type() {

    // Team Members
    register_post_type( 'team-member',
        array(
          'labels' => array(
            'name' => __( 'Team Members' ),
            'singular_name' => __( 'Team Member' )
          ),
          'public' => true,
          'has_archive' => false,
          "rewrite" => array("with_front" => false, "slug" => 'team-member'),
          'show_in_menu'    => 'lg_menu',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
        )
    );

    register_taxonomy(
      'team-category',
      'team-member',
      array(
        'label' => __( 'Category' ),
        'rewrite' => array( 'slug' => 'team-category', "with_front" => false ),
        'hierarchical' => true,
      )
    );

    add_submenu_page(
        'lg_menu',
        __('Team Category'), 
        __('Team Category'), 
        'edit_themes', 
        'edit-tags.php?taxonomy=team-category'
    );
}
add_action( 'init', 'create_post_type' );

?>