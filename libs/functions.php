<?php

	class lgFunctions{

		private static $instance = null;

		private function __construct(){
			add_action('wp_content_top', array($this, 'featured_banner_top'));
		}

		function featured_banner_top(){
			ob_start(); ?>
				<?php get_template_part( '/templates/template-parts/page/feature-slider' ); ?> 
			<?php echo ob_get_clean();
		}

		public static function getInstance(){
			if (self::$instance == null)
		    {
		      self::$instance = new lgFunctions();
		    }
		 
		    return self::$instance;
		}
	}

	lgFunctions::getInstance();


	function child_widgets_init() {

	// Footer Delta ~ First Item
		register_sidebar( 
			array(
				'name'          => esc_html__( 'Footer Delta', '_s' ),
				'id'            => 'footer-delta',
				'description'   => esc_html__( 'Add widgets here.', '_s' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>', 
			)		
		);

		// Footer Echo ~ First Item
		register_sidebar( 
			array(
				'name'          => esc_html__( 'Footer Echo', '_s' ),
				'id'            => 'footer-echo',
				'description'   => esc_html__( 'Add widgets here.', '_s' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>', 
			)		
		);

	}

	add_action( 'widgets_init', 'child_widgets_init' );

	function example_ajax_request() {
	 
	    if ( isset($_REQUEST) ) {
	        $fruit = $_REQUEST['fruit'];

	        if ( $fruit == 'Banana' ) {
	            $fruit = do_shortcode('[instagram-feed]');
	        }
	        echo $fruit;
	    }
	   die();
	}
	 
	add_action( 'wp_ajax_example_ajax_request', 'example_ajax_request' );

	add_action( 'wp_ajax_nopriv_example_ajax_request', 'example_ajax_request' );

	function example_ajax_enqueue() {
	    wp_localize_script(
	        'lg-script-child',
	        'example_ajax_obj',
	        array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) )
	    );
	}
	add_action( 'wp_enqueue_scripts', 'example_ajax_enqueue' );

 ?>